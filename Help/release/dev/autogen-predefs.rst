autogen-predefs
---------------

* When using :prop_tgt:`AUTOMOC`, the new variable
  :variable:`CMAKE_AUTOMOC_COMPILER_PREDEFINES` allows to default
  enable or disable the generation of the compiler pre definitions file
  ``moc_predefs.h``.

* When using :prop_tgt:`AUTOMOC`, the new boolean target property
  :prop_tgt:`AUTOMOC_COMPILER_PREDEFINES` allows to enable or disable the
  generation of the compiler pre definitions file ``moc_predefs.h``.
