protobuf-desc
-------------

* The :module:`FindProtobuf` module :command:`protobuf_generate_cpp` command
  gained a ``DESCRIPTORS`` option to generate descriptor files.
